import React from "react";
import { createStackNavigator, createSwitchNavigator, createAppContainer } from "react-navigation";
import HomeScreen from '../screens/home';
import LoginScreen from '../screens/login';
import ValidatorScreen from '../screens/validator';


const AppNavigator = createStackNavigator({
    Home: {
        screen: HomeScreen
    }
});

const LoginNavigator = createSwitchNavigator({
    ValidatorScreen,
    Login: {
        screen: LoginScreen
    },
    App: {
        screen: AppNavigator
    }
});

export default createAppContainer(LoginNavigator);
