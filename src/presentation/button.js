import React from 'react';
import {
    Button
} from 'react-native';

const CountButton = ({ onPress }) => {
    return <Button title='Contar' onPress={onPress} />
};

export default CountButton;
