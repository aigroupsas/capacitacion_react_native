import React from 'react';
import {
    Button
} from 'react-native';

const CleanButton = ({ onPress }) => {
    return <Button title='Resetear' onPress={onPress} />
};

export default CleanButton;
