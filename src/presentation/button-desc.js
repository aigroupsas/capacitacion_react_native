import React from 'react';
import {
    Button
} from 'react-native';

const DescButton = ({ onPress }) => {
    return <Button title='Restar' onPress={onPress} />
};

export default DescButton;
