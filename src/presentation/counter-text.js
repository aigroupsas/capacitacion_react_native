import React from 'react';
import {
    Text,
    StyleSheet
} from 'react-native';

const CounterText = (props) => {
    return <Text style={styles.text}>{props.text}</Text>
};

const styles = StyleSheet.create({
    text: {
        height: 50,
        width: '50%',
        fontSize: 30,
        margin: 15,
        textAlign: 'center',
        backgroundColor: '#000',
        color: 'red'
    }
});

export default CounterText;
