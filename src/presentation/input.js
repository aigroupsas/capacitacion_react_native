import React from 'react';
import {
    TextInput
} from 'react-native';

const NumberInput = (props) => {
    return <TextInput
        style={{height: 40}}
        placeholder="Ingrese el valor para sumar y restar"
        onChangeText={props.onChangeText}
        value={props.value}
    />
};

export default NumberInput;
