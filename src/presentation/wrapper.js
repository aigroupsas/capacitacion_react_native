import React from 'react';
import { View, StyleSheet } from 'react-native';

const Wrapper = ({ children }) => {
    return (<View style={styles.container}>
        {children}
    </View>);
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: '#CCC',
        padding: 10,
        margin: 10,
        marginHorizontal: 20,
        justifyContent: 'center',

    }
});

export default Wrapper;
