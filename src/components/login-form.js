import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Button } from "react-native";

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null
        };
    }

    render() {
        const { username, password } = this.state;
        const { loginHandler } = this.props;

        return (
            <View>
                <TextInput
                    value={username}
                    placeholder="Usuario"
                    onChangeText={(text) => this.setState({username: text})}
                />
                <TextInput
                    secureTextEntry={true}
                    value={password}
                    placeholder="Contraseña"
                    onChangeText={(text) => this.setState({password: text})}
                />
                <Button title="Login" onPress={() => loginHandler(username, password)}/>
            </View>
        );
    }
}

export default LoginForm;
