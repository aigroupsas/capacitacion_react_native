export const SET_USER = 'SET_USER';
export const setUser = (user = null) => {
    return {
        type: SET_USER,
        data: user
    };
};
