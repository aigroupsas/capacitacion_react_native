import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import LoginForm from "../components/login-form";
import { APILogin } from '../api';

class LoginScreen extends Component
{
    async login(username, password) {
        const { navigation } = this.props;

        /*APILogin(username, password).then(token => {
            console.log(token)
        }).catch(error => {
            console.log(error);
        });*/
        try {
            const token = await APILogin(username, password);
            AsyncStorage.setItem('@token', token);
            AsyncStorage.setItem('@username', username);
            navigation.navigate('Home', {username});
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        return <LoginForm loginHandler={(username, password) => this.login(username, password)} />
    }
}

export default LoginScreen;
