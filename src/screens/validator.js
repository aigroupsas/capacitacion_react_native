import React, { Component, Fragment } from 'react';
import { ActivityIndicator, Text } from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {setUser} from "../actions";

class ValidatorScreen extends Component
{
    componentDidMount(): void {
        const { navigation, setUser } = this.props;
        AsyncStorage.getItem('@token').then(token => {
            AsyncStorage.getItem('@username').then(username => {
                if(token) {
                    setUser(username);
                    navigation.navigate('App');
                } else {
                    navigation.navigate('Login');
                }
            });
        });
    }

    render() {
        return <Fragment>
            <Text>Cargando...</Text>
            <ActivityIndicator />
        </Fragment>
    }
}

export default connect(null, (dispatch) => {
    return {
        setUser: (username) => dispatch(setUser(username))
    };
})(ValidatorScreen);
