import React, { Component } from 'react';
import { Text } from 'react-native';
import Wrapper from '../presentation/wrapper';
import { connect } from 'react-redux';
import CountButton from '../presentation/button';
import DescButton from '../presentation/button-desc';
import CleanButton from '../presentation/button-clean';
import CounterText from '../presentation/counter-text';
import NumberInput from '../presentation/input';

class Home extends Component
{
    constructor(props) {
        super(props);

        this.state = {
            count: 1,
            state1: 0,
            state2: null
        };

        this.incrementCounter = this.incrementCounter.bind(this);
        this.decrementCounter = this.decrementCounter.bind(this);
        this.cleanCounter = this.cleanCounter.bind(this);

    }

    incrementCounter() {
        let { count } = this.state;
        let { state1 } = this.state;
        count = count + state1;
        this.setState({count})
    }

    decrementCounter() {
        let { count } = this.state;
        count--;
        this.setState({count})
    }

    cleanCounter() {
        let { count } = this.state;
        count = 0;
        this.setState({count})
    }

    render() {
        const { count } = this.state;
        const { state1 } = this.state;
        const { user } = this.props;
        console.log(user);

        return (<Wrapper >
            <Text>Bienvenido {user}</Text>
            <CountButton
                onPress={this.incrementCounter}
            />
            <DescButton
                onPress={this.decrementCounter}
            />
            <CleanButton
                onPress={this.cleanCounter}
            />
            <CounterText text={count} />
            <NumberInput
                value={`${state1}`}
                onChangeText={(text) => this.setState({state1: parseInt(text) || 0})}
            />
        </Wrapper>);
    }
}

export default connect(function(state) {
    return {
        user: state.user
    };
})(Home);
