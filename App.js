/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import AppNavigator from './src/navigation/navigator';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducers from './src/reducers'

let store = createStore(reducers);

function App () {
  return (<Provider store={store}>
      <AppNavigator />
  </Provider>);
}

export default App;
